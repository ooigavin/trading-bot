from flask import Flask
from celery import Celery
from redis import Redis

app = Flask(__name__)
app.config['CELERY_BROKER_URL'] = 'redis://redis:6379/0'
app.config['result_backend'] = 'redis://redis:6379/0'

celery_client = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
celery_client.conf.update(app.config)

cache = Redis(host='redis', port=6379)

from . import views
