import os
import json
from flask import request

from . import app
from . import cache

from async_tasks import tasks

from misc import oanda_utils as oa

from trading_machine.trading_constants import Instrument, TimePeriods, LTMA_PERIOD, STMA_PERIOD, RSI_PERIOD, ADX_PERIOD
from trading_machine.trading_indicators import new_moving_average, new_adx, new_rsi


def auth():
  return request.args.get('secret') == os.environ.get('AUTH_SECRET')


@app.route('/async_tasks')
def celery():
  if auth():
    env = request.args.get('env')
    tasks.run_machine.delay(Instrument.EUR_USD, env)
    return 'async_tasks called'
  else:
    return {'message': 'Invalid request'}, 400


@app.route('/cache')
def get_cache():
  if auth():
    instrument = request.args.get('instrument')
    if not instrument:
      return {'message': 'instrument required'}, 400

    instrument_cache = json.loads(cache.get(instrument))
    return instrument_cache, 200
  else:
    return {'message': 'Invalid request'}, 400


@app.route('/clear-cache')
def clear_cache():
  if auth():
    instrument = request.args.get('instrument')
    if not instrument:
      return {'message': 'instrument required'}, 400

    c = cache.get(instrument)
    c = json.loads(c)
    c['prices'] = None
    cache.set(instrument, json.dumps(c))
    return 'cleared'
  else:
    return {'message': 'Invalid request'}, 400


@app.route('/set-cache', methods=['POST'])
def set_cache():
  if auth():
    req = request.get_json(force=True)
    instrument = req.get('instrument')
    cache_values = cache.get(instrument)
    if cache_values:
      cache_values = json.loads(cache_values)
      cache_values.update(req.get('cache_values'))
    else:
      cache_values = req.get('cache_values')

    cache.set(instrument, json.dumps(cache_values))
    return {'message': 'updated values'}, 200
  else:
    return {'message': 'Invalid request'}, 400


@app.route('/del-cache', methods=['POST'])
def del_cache():
  if auth():
    req = request.get_json(force=True)
    instrument = req.get('instrument')
    fields = req.get('fields')
    if instrument:
      cache_values = cache.get(instrument)
      if cache_values:
        cache_values = json.loads(cache_values)
        for field in fields:
          cache_values.pop(field, None)
      cache.set(instrument, json.dumps(cache_values))
    else:
      for field in fields:
        cache.delete(field)

    return {'message': 'Deleted values'}, 200
  else:
    return {'message': 'Invalid request'}, 400


@app.route('/flush-queue', methods=['DELETE'])
def flush_queue():
  if auth():
    for k in cache.keys():
      if 'celery-task' in str(k):
        cache.delete(k)
    return {'message': 'Queue flushed'}, 200
  else:
    return {'message': 'Invalid request'}, 400


@app.route('/refetch-candles')
def refetch_candles():
  if auth():
    instrument = request.args.get('instrument')
    env = request.args.get('env')
    count = int(request.args.get('count'))
    cache_key = f'{env}_{instrument}'
    instrument_values = json.loads(cache.get(cache_key))
    candles = oa.fetch_candles(TimePeriods.H1, instrument, count, env)
    for candle in candles:
      print(candle['time'])
      prices = instrument_values['prices']
      if candle['time'] == prices[-1]['time']:
        continue
      prices.append(candle)
      prev, curr = prices[-2]['candle'], prices[-1]['candle']
      old_ltma = instrument_values['ltma']
      old_stma = instrument_values['stma']
      old_atr = instrument_values['atr']
      old_smoothed_tr = instrument_values['smoothed_tr']
      old_smoothed_pos_dm = instrument_values['smoothed_pos_dm']
      old_smoothed_neg_dm = instrument_values['smoothed_neg_dm']
      old_adx = instrument_values['adx']
      old_avg_gain = instrument_values['avg_gain']
      old_avg_loss = instrument_values['avg_loss']
      ltma = new_moving_average(old_ltma, prices, LTMA_PERIOD)
      stma = new_moving_average(old_stma, prices[-(STMA_PERIOD + 1):], STMA_PERIOD)
      pos_di, smoothed_pos_dm, neg_di, smoothed_neg_dm, atr, smoothed_tr, adx_val = new_adx(prev, curr,
                                                                                            old_smoothed_pos_dm,
                                                                                            old_smoothed_neg_dm,
                                                                                            old_atr, old_smoothed_tr,
                                                                                            old_adx, ADX_PERIOD)
      avg_gain, avg_loss, rsi_val = new_rsi(prev, curr, old_avg_gain, old_avg_loss, RSI_PERIOD)
      prices.pop(0)

      instrument_values['ltma'] = ltma
      instrument_values['stma'] = stma
      instrument_values['atr'] = atr
      instrument_values['smoothed_tr'] = smoothed_tr
      instrument_values['smoothed_pos_dm'] = smoothed_pos_dm
      instrument_values['smoothed_neg_dm'] = smoothed_neg_dm
      instrument_values['pos_di'] = pos_di
      instrument_values['neg_di'] = neg_di
      instrument_values['adx'] = adx_val
      instrument_values['avg_gain'] = avg_gain
      instrument_values['avg_loss'] = avg_loss
      instrument_values['rsi'] = rsi_val

    cache.set(cache_key, json.dumps(instrument_values))
    return {'message': f'Updated {cache_key} with {count} candles'}, 200
  else:
    return {'message': 'Invalid request'}, 400
