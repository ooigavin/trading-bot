from datetime import timedelta

from app import celery_client
from celery.schedules import BaseSchedule, schedstate

from trading_machine.trading_constants import Instrument, Env


class ForexSchedule(BaseSchedule):

  def __init__(self, interval, start, end, second, app):
    self.start = start
    self.end = end
    self.interval = interval
    self.sec = second
    super().__init__(app=app)

  @staticmethod
  def _get_delta(start, end):
    if start < end:
      return end - start
    else:
      return 7 - (start - end)  # if same day, delta is one week

  def remaining_estimate(self, last_run_at):
    delta_to_start = self._get_delta(self.start[0], last_run_at.weekday())
    next_start = last_run_at.replace(hour=self.start[1], minute=0, second=self.sec)
    if delta_to_start == 7 and last_run_at >= next_start:  # if now is same day, but just after start
      last_start = next_start
    else:
      last_start = last_run_at.replace(hour=self.start[1], minute=0, second=self.sec) - timedelta(days=delta_to_start)

    closest_end = last_start.replace(hour=self.end[1]) + timedelta(days=self._get_delta(self.start[0], self.end[0]))
    next_run = last_run_at.replace(minute=0, second=self.sec) + timedelta(seconds=self.interval)
    # print(f'\nlast start: {last_start}\nclosest_end: {closest_end}\nnext run: {next_run}\nnow: {self.now()}')
    # if time is within working range, find time to next interval
    if next_run < closest_end:
      return next_run - self.now()
    # else find time to next start
    else:
      next_start = last_start + timedelta(days=7)
      return next_start - self.now()

  def is_due(self, last_run_at):
    last_run_at = self.maybe_make_aware(last_run_at)
    rem_delta = self.remaining_estimate(last_run_at)
    remaining_s = max(rem_delta.total_seconds(), 0)

    if remaining_s == 0:
      return schedstate(is_due=True, next=self.interval)
    return schedstate(is_due=False, next=remaining_s)


celery_client.conf.beat_schedule = {
  'eur-usd': {
    'task': 'async_tasks.tasks.run_machine',
    'schedule': ForexSchedule(start=(6, 21), end=(4, 22), interval=3600, app=celery_client, second=5),
    'args': (Instrument.EUR_USD, Env.LIVE)
  },
  'aud-usd': {
    'task': 'async_tasks.tasks.run_machine',
    'schedule': ForexSchedule(start=(6, 21), end=(4, 22), interval=3600, app=celery_client, second=5),
    'args': (Instrument.AUD_USD, Env.LIVE)
  },
  'nzd-usd': {
    'task': 'async_tasks.tasks.run_machine',
    'schedule': ForexSchedule(start=(6, 21), end=(4, 22), interval=3600, app=celery_client, second=6),
    'args': (Instrument.NZD_USD, Env.LIVE)
  },
  'gbp-usd': {
    'task': 'async_tasks.tasks.run_machine',
    'schedule': ForexSchedule(start=(6, 21), end=(4, 22), interval=3600, app=celery_client, second=6),
    'args': (Instrument.GBP_USD, Env.LIVE)
  },
  'eur-usd-demo': {
    'task': 'async_tasks.tasks.run_machine',
    'schedule': ForexSchedule(start=(6, 21), end=(4, 22), interval=3600, app=celery_client, second=7),
    'args': (Instrument.EUR_USD, Env.DEMO)
  },
  'aud-usd-demo': {
    'task': 'async_tasks.tasks.run_machine',
    'schedule': ForexSchedule(start=(6, 21), end=(4, 22), interval=3600, app=celery_client, second=7),
    'args': (Instrument.AUD_USD, Env.DEMO)
  },
  'nzd-usd-demo': {
    'task': 'async_tasks.tasks.run_machine',
    'schedule': ForexSchedule(start=(6, 21), end=(4, 22), interval=3600, app=celery_client, second=7),
    'args': (Instrument.NZD_USD, Env.DEMO)
  },
  'gbp-usd-demo': {
    'task': 'async_tasks.tasks.run_machine',
    'schedule': ForexSchedule(start=(6, 21), end=(4, 22), interval=3600, app=celery_client, second=7),
    'args': (Instrument.GBP_USD, Env.DEMO)
  }
}
