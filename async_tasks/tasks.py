import os
from datetime import datetime

from app import celery_client

from misc import telegram_utils as tele

from trading_machine.state_machines.trading_machine import TradingMachine
from trading_machine.context.trading_bot import TradingBot


@celery_client.task()
def run_machine(instrument, env):
  machine = TradingMachine()
  bot = TradingBot(machine, instrument, env)
  bot.start()


@celery_client.task()
def hello():
  print(f'hi: {datetime.now().strftime("%H:%M:%S")}')
  # tele.send_bot_message(f'hi: {datetime.now().strftime("%H:%M:%S")}', os.environ.get('TELE_CHAT_ID'))
