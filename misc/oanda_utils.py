import requests
import os
from datetime import datetime, timedelta
import csv

from trading_machine.trading_constants import Env

requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL:@SECLEVEL=1'


def get_oanda_env(env):
  if env == Env.DEMO:
    url = 'https://api-fxpractice.oanda.com'
    acc_id = os.environ.get('OANDA_ACC_ID_DEMO')
    token = os.environ.get('OANDA_TOKEN_DEMO')
  else:
    url = 'https://api-fxtrade.oanda.com'
    acc_id = os.environ.get('OANDA_ACC_ID_LIVE')
    token = os.environ.get('OANDA_TOKEN_LIVE')

  return url, acc_id, token


def fetch_candles(period, instrument, count, env=Env.DEMO):
  url, acc_id, token = get_oanda_env(env)

  def _clean_candles(candle_data):
    candle_data.pop('complete')
    candle_data.pop('volume')
    dt = datetime.strptime(candle_data.pop('time').split('.')[0], '%Y-%m-%dT%H:%M:%S') + timedelta(hours=8)
    candle_data['time'] = dt.strftime('%d/%m T%H:%M')
    candle_data['bid'] = float(candle_data.pop('bid')['c'])
    candle_data['ask'] = float(candle_data.pop('ask')['c'])
    candle_data['candle'] = {
      key: float(val)
      for key, val in candle_data.pop('mid').items()
    }

    return candle_data

  endpt = f'{url}/v3/instruments/{instrument}/candles'
  count += 1  # to account for incomplete candles
  res = requests.get(
    endpt,
    params={'count': count, 'granularity': period, 'smooth': True, 'price': 'BAM'},
    headers={'Authorization': f'Bearer {token}', 'Content-Type': 'application/json'}
  )

  data = [_clean_candles(candle) for candle in res.json()['candles'] if candle['complete']]
  if len(data) == count:
    data.pop(0)  # remove earliest candle if the count is +1
  return data


def get_acc_size(env=Env.DEMO):
  url, acc_id, token = get_oanda_env(env)
  endpt = f'{url}/v3/accounts/{acc_id}'
  res = requests.get(
    endpt,
    headers={'Authorization': f'Bearer {token}', 'Content-Type': 'application/json'}
  )
  balance = res.json()['account']['balance']

  return float(balance)


def create_market_order(instrument, stop_loss, units, env=Env.DEMO):
  url, acc_id, token = get_oanda_env(env)
  endpt = f'{url}/v3/accounts/{acc_id}/orders'
  payload = {
    'order': {
      'type': 'MARKET',
      'instrument': instrument,
      'units': str(units),
      'guaranteedStopLossOnFill': {'price': str(stop_loss)}
    }
  }
  resp = requests.post(
    endpt,
    json=payload,
    headers={'Authorization': f'Bearer {token}', 'Content-Type': 'application/json'}
  )
  return resp.json()


def get_position(instrument, env=Env.DEMO):
  url, acc_id, token = get_oanda_env(env)
  endpt = f'{url}/v3/accounts/{acc_id}/positions/{instrument}'
  res = requests.get(
    endpt,
    headers={'Authorization': f'Bearer {token}', 'Content-Type': 'application/json'}
  )

  return res.json()


def close_position(instrument, long=False, short=False, env=Env.DEMO):
  url, acc_id, token = get_oanda_env(env)
  payload = {
    'longUnits': 'ALL' if long else 'NONE',
    'shortUnits': 'ALL' if short else 'NONE'
  }
  endpt = f'{url}/v3/accounts/{acc_id}/positions/{instrument}/close'
  res = requests.put(
    endpt,
    json=payload,
    headers={'Authorization': f'Bearer {token}', 'Content-Type': 'application/json'}
  )

  return res.json()


def get_transaction(trans_id, env=Env.DEMO):
  url, acc_id, token = get_oanda_env(env)
  endpt = f'{url}/v3/accounts/{acc_id}/transactions/{trans_id}'
  res = requests.get(
    endpt,
    headers={'Authorization': f'Bearer {token}', 'Content-Type': 'application/json'}
  )

  return res.json()


def generate_candlestick_data(instrument, end, start, period, filename, env=Env.DEMO):
  url, acc_id, token = get_oanda_env(env)
  def _format_date(date_string):
    dt = datetime.strptime(date_string.split('.')[0], '%Y-%m-%dT%H:%M:%S') + timedelta(hours=8)
    return dt.strftime('%d/%m/%y T%H:%M')

  with open(f'csv_files/{filename}.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    curr = start
    while curr < end:
      curr_month, curr_year = curr.month, curr.year
      if curr_month == 12:
        next_date = curr.replace(year=curr_year+1, month=1)
      else:
        next_date = curr.replace(month=curr_month+1)
      endpt = f'{url}/v3/instruments/{instrument}/candles'
      param = {'from': curr.strftime('%Y-%m-%d'), 'to': next_date.strftime('%Y-%m-%d'), 'granularity': period, 'smooth': True, 'price': 'BAM'}
      print(f'\n{param}')
      res = requests.get(
        endpt,
        params=param,
        headers={'Authorization': f'Bearer {token}', 'Content-Type': 'application/json'}
      )
      for candle in res.json()['candles']:
        writer.writerow(
          [_format_date(candle['time']), candle['bid']['c'], candle['ask']['c'], candle['mid']['o'], candle['mid']['c'], candle['mid']['l'], candle['mid']['h']]
        )
      curr = next_date
