import requests
import os

telegram_url = 'https://api.telegram.org/bot'
token = os.environ.get('TELE_BOT_TOKEN')


def send_bot_message(msg, chat_id):
  send_text = f'{telegram_url}{token}/sendMessage?chat_id={chat_id}&text={msg}'

  requests.get(send_text)
