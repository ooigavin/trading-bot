from unittest import TestCase, mock
from datetime import datetime, timezone

from app import celery_client

from async_tasks.schedules import ForexSchedule


class ForexScheduleTest(TestCase):

  def setUp(self):
    now_patcher = mock.patch('tests.test_schedules.celery_client.now')
    self.now = now_patcher.start()
    self.addCleanup(now_patcher.stop)

  def test_start_before_end_lastrun_after_start_within_due(self):
    self.now.return_value = datetime(2020, 9, 10, 16, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(2, 14), end=(5, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 10, 15)
    due, next_time = schedule.is_due(last_run)

    self.assertTrue(due)
    self.assertEqual(60, int(next_time/60))

  def test_start_before_end_lastrun_after_start_within_not_due(self):
    self.now.return_value = datetime(2020, 9, 10, 15, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(2, 14), end=(5, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 10, 15)
    due, next_time = schedule.is_due(last_run)

    self.assertFalse(due)
    self.assertEqual(25, int(next_time/60))

  def test_start_before_end_outside_due(self):
    self.now.return_value = datetime(2020, 9, 10, 15, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(2, 14), end=(5, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 8, 15)
    due, next_time = schedule.is_due(last_run)

    self.assertTrue(due)
    self.assertEqual(60, int(next_time/60))

  def test_start_before_end_outside_not_due(self):
    self.now.return_value = datetime(2020, 9, 9, 13, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(2, 14), end=(5, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 7, 15)
    due, next_time = schedule.is_due(last_run)

    self.assertFalse(due)
    self.assertEqual(25, int(next_time/60))

  def test_start_before_end_lastrun_sameday_after_start_not_due(self):
    # will always be within
    self.now.return_value = datetime(2020, 9, 9, 15, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(2, 14), end=(5, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 9, 15)
    due, next_time = schedule.is_due(last_run)

    self.assertFalse(due)
    self.assertEqual(25, int(next_time/60))

  def test_start_before_end_lastrun_sameday_after_start_due(self):
    # will always be within
    self.now.return_value = datetime(2020, 9, 9, 16, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(2, 14), end=(5, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 9, 15)
    due, next_time = schedule.is_due(last_run)

    self.assertTrue(due)
    self.assertEqual(60, int(next_time/60))

  def test_start_before_end_lastrun_sameday_before_start_due(self):
    # will always be outside
    self.now.return_value = datetime(2020, 9, 9, 15, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(2, 14), end=(5, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 9, 9)
    due, next_time = schedule.is_due(last_run)

    self.assertTrue(due)
    self.assertEqual(60, int(next_time/60))

  def test_start_before_end_lastrun_sameday_before_start_not_due(self):
    # will always be outside
    self.now.return_value = datetime(2020, 9, 9, 12, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(2, 14), end=(5, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 9, 9)
    due, next_time = schedule.is_due(last_run)

    self.assertFalse(due)
    self.assertEqual(85, int(next_time/60))

  def test_end_before_start_lastrun_after_start_within_due(self):
    self.now.return_value = datetime(2020, 9, 13, 12, 35, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(4, 14), end=(1, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 12, 9)
    due, next_time = schedule.is_due(last_run)

    self.assertTrue(due)
    self.assertEqual(60, int(next_time/60))

  def test_end_before_start_lastrun_after_start_within_not_due(self):
    self.now.return_value = datetime(2020, 9, 12, 9, 15, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(4, 14), end=(1, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 12, 9)
    due, next_time = schedule.is_due(last_run)

    self.assertFalse(due)
    self.assertEqual(45, int(next_time/60))

  def test_end_before_start_lastrun_before_start_outside_due(self):
    self.now.return_value = datetime(2020, 9, 11, 14, 15, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(4, 14), end=(1, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 10, 9)
    due, next_time = schedule.is_due(last_run)

    self.assertTrue(due)
    self.assertEqual(60, int(next_time/60))

  def test_end_before_start_lastrun_before_start_outside_not_due(self):
    self.now.return_value = datetime(2020, 9, 11, 12, 15, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(4, 14), end=(1, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 10, 9)
    due, next_time = schedule.is_due(last_run)

    self.assertFalse(due)
    self.assertEqual(105, int(next_time/60))

  def test_end_before_start_lastrun_sameday_after_start_not_due(self):
    # will always be within
    self.now.return_value = datetime(2020, 9, 11, 14, 45, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(4, 14), end=(1, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 11, 14, 12)
    due, next_time = schedule.is_due(last_run)

    self.assertFalse(due)
    self.assertEqual(15, int(next_time/60))

  def test_end_before_start_lastrun_sameday_after_start_due(self):
    # will always be within
    self.now.return_value = datetime(2020, 9, 11, 20, 15, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(4, 14), end=(1, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 11, 19)
    due, next_time = schedule.is_due(last_run)

    self.assertTrue(due)
    self.assertEqual(60, int(next_time/60))

  def test_end_before_start_lastrun_sameday_before_start_due(self):
    # will always be outside
    self.now.return_value = datetime(2020, 9, 11, 14, 15, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(4, 14), end=(1, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 11, 8, 12)
    due, next_time = schedule.is_due(last_run)

    self.assertTrue(due)
    self.assertEqual(60, int(next_time/60))

  def test_end_before_start_lastrun_sameday_before_start_not_due(self):
    # will always be outside
    self.now.return_value = datetime(2020, 9, 11, 11, 15, tzinfo=timezone.utc)
    schedule = ForexSchedule(interval=3600, start=(4, 14), end=(1, 7), second=5, app=celery_client)
    last_run = datetime(2020, 9, 11, 8, 12)
    due, next_time = schedule.is_due(last_run)

    self.assertFalse(due)
    self.assertEqual(165, int(next_time/60))
