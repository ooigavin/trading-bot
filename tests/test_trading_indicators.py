from unittest import TestCase

from trading_machine import trading_indicators as ti
from trading_machine.trading_constants import MarketSentiment


class IndicatorsTestCase(TestCase):

  def setUp(self):
    self.prices = [
      {'candle': {'o': 1.18424, 'h': 1.18452, 'l': 1.18255, 'c': 1.18259}},
      {'candle': {'o': 1.1826, 'h': 1.18271, 'l': 1.18222, 'c': 1.18246}},
      {'candle': {'o': 1.18133, 'h': 1.18145, 'l': 1.18124, 'c': 1.1813}},
      {'candle': {'o': 1.18028, 'h': 1.18028, 'l': 1.17984, 'c': 1.17995}},
      {'candle': {'o': 1.17993, 'h': 1.18045, 'l': 1.17991, 'c': 1.18042}},
      {'candle': {'o': 1.17976, 'h': 1.17989, 'l': 1.17974, 'c': 1.17986}},
      {'candle': {'o': 1.1792, 'h': 1.17928, 'l': 1.1791, 'c': 1.17912}},
      {'candle': {'o': 1.17918, 'h': 1.17918, 'l': 1.17888, 'c': 1.17908}},
      {'candle': {'o': 1.17923, 'h': 1.17923, 'l': 1.17902, 'c': 1.17902}},
      {'candle': {'o': 1.17914, 'h': 1.17914, 'l': 1.179, 'c': 1.17908}},
      {'candle': {'o': 1.17942, 'h': 1.17944, 'l': 1.17935, 'c': 1.17936}},
      {'candle': {'o': 1.17936, 'h': 1.17949, 'l': 1.17934, 'c': 1.17944}},
      {'candle': {'o': 1.17989, 'h': 1.18006, 'l': 1.17982, 'c': 1.17985}},
      {'candle': {'o': 1.18064, 'h': 1.18084, 'l': 1.1806, 'c': 1.18065}},
      {'candle': {'o': 1.18048, 'h': 1.18055, 'l': 1.18048, 'c': 1.18051}},
      {'candle': {'o': 1.18062, 'h': 1.18122, 'l': 1.18062, 'c': 1.18121}},
      {'candle': {'o': 1.18104, 'h': 1.18116, 'l': 1.181, 'c': 1.18105}},
      {'candle': {'o': 1.18098, 'h': 1.18154, 'l': 1.1809, 'c': 1.18146}},
      {'candle': {'o': 1.18, 'h': 1.1804, 'l': 1.1798, 'c': 1.17999}},
      {'candle': {'o': 1.18268, 'h': 1.1834, 'l': 1.1826, 'c': 1.18292}},
      {'candle': {'o': 1.18237, 'h': 1.18238, 'l': 1.1821, 'c': 1.18233}},
      {'candle': {'o': 1.18284, 'h': 1.18304, 'l': 1.18278, 'c': 1.18283}},
      {'candle': {'o': 1.1826, 'h': 1.18269, 'l': 1.18246, 'c': 1.18268}},
      {'candle': {'o': 1.18132, 'h': 1.18188, 'l': 1.1813, 'c': 1.18174}},
      {'candle': {'o': 1.18167, 'h': 1.18262, 'l': 1.18152, 'c': 1.18206}},
      {'candle': {'o': 1.18308, 'h': 1.1833, 'l': 1.18254, 'c': 1.18294}},
      {'candle': {'o': 1.1823, 'h': 1.18262, 'l': 1.18212, 'c': 1.18258}},
      {'candle': {'o': 1.18178, 'h': 1.18195, 'l': 1.18158, 'c': 1.18185}},
      {'candle': {'o': 1.1812, 'h': 1.18142, 'l': 1.18113, 'c': 1.18141}},
      {'candle': {'o': 1.18789, 'h': 1.19113, 'l': 1.18674, 'c': 1.18963}},
      {'candle': {'o': 1.18963, 'h': 1.19070, 'l': 1.18878, 'c': 1.19005}},
      {'candle': {'o': 1.19005, 'h': 1.19059, 'l': 1.18878, 'c': 1.18884}},
      {'candle': {'o': 1.18884, 'h': 1.18928, 'l': 1.18844, 'c': 1.18867}},
      {'candle': {'o': 1.18867, 'h': 1.18938, 'l': 1.18841, 'c': 1.18906}},
      {'candle': {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.18982}}
    ]


class MovingAverageTests(IndicatorsTestCase):

  def setUp(self):
    super().setUp()

  def test_hlc(self):
    hlc = ti.get_hlc(self.prices[0]['candle'])
    self.assertEqual(1.18322, round(hlc, 6))

  def test_moving_avg(self):
    ma = ti.moving_average(self.prices[:10])
    self.assertEqual(1.18035, ma)

  def test_new_moving_avg(self):
    new_ma = ti.new_moving_average(1.18035, self.prices, 30)
    self.assertEqual(1.180555, new_ma)


class AverageTrueRangeTests(IndicatorsTestCase):

  def setUp(self):
    super().setUp()

  def test_true_range_m1(self):
    prev = {'o': 1.18867, 'h': 1.19538, 'l': 1.18841, 'c': 1.18906}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.19282}
    self.assertEqual(curr['h'] - curr['l'], ti.true_range(prev, curr))

  def test_true_range_m2(self):
    prev = {'o': 1.18867, 'h': 1.18938, 'l': 1.18841, 'c': 1.18306}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.18982}
    self.assertEqual(abs(curr['h'] - prev['c']), ti.true_range(prev, curr))

  def test_true_range_m3(self):
    prev = {'o': 1.18867, 'h': 1.18938, 'l': 1.18841, 'c': 1.19506}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.18982}
    self.assertEqual(abs(curr['l'] - prev['c']), ti.true_range(prev, curr))

  def test_atr(self):
    self.assertEqual(0.0012, ti.average_true_range(self.prices))

  def test_new_atr(self):
    prev = {'o': 1.18867, 'h': 1.18938, 'l': 1.18841, 'c': 1.19506}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.18982}
    atr, tr = ti.new_atr(0.0012, prev, curr, 20)
    self.assertEqual(0.0015, round(atr, 4))


class PriceRejectionTests(IndicatorsTestCase):

  def setUp(self):
    super().setUp()

  def test_bullish_rejection_outside_window(self):
    prev = {'o': 1.18867, 'h': 1.19538, 'l': 1.18841, 'c': 1.18506}
    curr = {'o': 1.18506, 'h': 1.18992, 'l': 1.18837, 'c': 1.19282}
    self.assertFalse(ti.check_for_rejection(prev, curr, MarketSentiment.BULL, 1.17))

  def test_bullish_rejection_absent(self):
    prev = {'o': 1.18867, 'h': 1.19538, 'l': 1.18841, 'c': 1.18906}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.18282}
    self.assertFalse(ti.check_for_rejection(prev, curr, MarketSentiment.BULL, 1.184))

  def test_bullish_rejection_smaller_percentage(self):
    prev = {'o': 1.18067, 'h': 1.19538, 'l': 1.18841, 'c': 1.18806}
    curr = {'o': 1.18806, 'h': 1.18992, 'l': 1.18837, 'c': 1.18982}
    self.assertFalse(ti.check_for_rejection(prev, curr, MarketSentiment.BULL, 1.89))

  def test_bullish_rejection_success(self):
    prev = {'o': 1.18867, 'h': 1.19538, 'l': 1.18841, 'c': 1.18806}
    curr = {'o': 1.18806, 'h': 1.18992, 'l': 1.18837, 'c': 1.19282}
    self.assertTrue(ti.check_for_rejection(prev, curr, MarketSentiment.BULL, 1.89))

  def test_bearish_rejection_outside_window(self):
    prev = {'o': 1.18797, 'h': 1.19538, 'l': 1.18841, 'c': 1.18906}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.18782}
    self.assertFalse(ti.check_for_rejection(prev, curr, MarketSentiment.BEAR, 1.199))

  def test_bearish_rejection_absent(self):
    prev = {'o': 1.18867, 'h': 1.19538, 'l': 1.18841, 'c': 1.18906}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.19182}
    self.assertFalse(ti.check_for_rejection(prev, curr, MarketSentiment.BEAR, 1.17))

  def test_bearish_rejection_smaller_percentage(self):
    prev = {'o': 1.18167, 'h': 1.19538, 'l': 1.18841, 'c': 1.18906}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.18782}
    self.assertFalse(ti.check_for_rejection(prev, curr, MarketSentiment.BEAR, 1.190))

  def test_bearish_rejection_success(self):
    prev = {'o': 1.18797, 'h': 1.19538, 'l': 1.18841, 'c': 1.18906}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.18782}
    self.assertTrue(ti.check_for_rejection(prev, curr, MarketSentiment.BEAR, 1.1890))

  def test_zero_division(self):
    prev = {'o': 1.18867, 'h': 1.19538, 'l': 1.18841, 'c': 1.18867}
    curr = {'o': 1.18906, 'h': 1.18992, 'l': 1.18837, 'c': 1.19282}
    self.assertFalse(ti.check_for_rejection(prev, curr, MarketSentiment.BULL, 1.18))
