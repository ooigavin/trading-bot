import csv
from itertools import islice

from trading_machine.context.context import BaseContext
from trading_machine.state_machines import backtesting_adx

from trading_machine.trading_constants import STMA_PERIOD, RSI_PERIOD, ADX_PERIOD, MarketSentiment
from trading_machine.trading_indicators import moving_average, adx, rsi


class Backtester(BaseContext):

  def __init__(self, machine, instrument, input_file, output_file=None):
    super().__init__(machine)

    self.instrument = instrument
    self.file_gen = (self._format_candle(row) for row in open(f'csv_files/{input_file}.csv'))
    if output_file is None:
      output_file = f'{input_file}-result'
    self.output_file = open(f'csv_files/{output_file}.csv', 'w', newline='')
    self.output_writer = csv.writer(self.output_file)
    self.output_writer.writerow(
      ['Instrument', 'Acc Balance', 'Position', 'Entry Time', 'Entry Price', 'ADX', 'Pos DI', 'Neg DI', 'RSI', 'Exit Time', 'Exit Price', 'Stop Loss', 'Units', 'Stopped Out', 'Profit/Loss']
    )
    new_prices = self.fetch_candles(50)
    self.record = {}
    self.losing_streak = 0
    self.losing_amt = 0

    pos_di, smoothed_pos_dm, neg_di, smoothed_neg_dm, atr, smoothed_tr, adx_val = adx(new_prices[-(ADX_PERIOD * 2):])
    avg_gain, avg_loss, rsi_val = rsi(new_prices[-RSI_PERIOD+1:])
    if pos_di > neg_di:
      trend = MarketSentiment.BULL
    else:
      trend = MarketSentiment.BEAR

    self._cache = {
      'acc_balance': 200.0,
      'entered_trade': False,
      'entry_allowed': True,
      'strong_rejections': 0,
      'entry_price': 0,
      'stop_loss': 0,
      'losses': 0,
      'market_trend': trend,
      'prices': new_prices,
      'stma': moving_average(new_prices[-STMA_PERIOD:]),
      'ltma': moving_average(new_prices),
      'atr': atr,
      'smoothed_tr': smoothed_tr,
      'smoothed_pos_dm': smoothed_pos_dm,
      'smoothed_neg_dm': smoothed_neg_dm,
      'pos_di': pos_di,
      'neg_di': neg_di,
      'adx': adx_val,
      'avg_gain': avg_gain,
      'avg_loss': avg_loss,
      'rsi': rsi_val,
      'total_trades': 0,
      'winning_trades': 0,
      'profit_from_trades': 0,
      'losing_trades': 0,
      'loss_from_trades': 0,
      'max_profit': 0,
      'max_loss': 0,
      'longest_losing_streak': 0,
      'losing_streak_losses': 0
    }

  def start(self):
    msg = ''
    while self.get_state().name != 'END':
      new = f'\nhandling: {self.get_state().name}'
      # msg += new
      self.get_state().handle(self)

    # add cache values
    # msg += f'\n\nCached values\nacc balance: {str(self.get_cache("acc_balance"))}'
    last = self.get_cache("prices")[-1]
    msg += f'\nlast: {last["time"]}, open: {last["candle"]["o"]}, close: {last["candle"]["c"]}'
    for field, val in self._cache.items():
      # if field != 'prices':
      if field in ['rsi', 'pos_di', 'neg_di', 'adx']:
        msg += f'\n{field}: {val}'

    # print(msg)

  def _format_candle(self, row):
    row_list = row.split(',')
    return {
      'time': row_list[0],
      'bid': float(row_list[1]),
      'ask': float(row_list[2]),
      'candle': {'o': float(row_list[3]), 'c': float(row_list[4]), 'l': float(row_list[5]), 'h': float(row_list[6])}
    }

  def run(self):
    self.start()
    self.machine.reset()

  def backtest(self):
    count = 0
    try:
      while self.get_cache('acc_balance') > 5:
        count += 1
        self.run()
      prices = self.get_cache('prices')
      print(f'ran out of money: {prices[-1]["time"]}')
    except IndexError:
      pass
    self.conclude()

  def fetch_candles(self, no):
    return [row for row in islice(self.file_gen, no)]

  def get_cache(self, key):
    return self._cache.get(key)

  def set_cache(self, key, value):
    self._cache[key] = value

  def save_cache(self):
    pass

  def _record_trade(self):
    self.output_writer.writerow(
      [
        self.instrument,
        self.get_cache('acc_balance'),
        self.record['position'],
        self.record['entry_time'],
        self.record['entry_price'],
        self.record['adx'],
        self.record['pos_di'],
        self.record['neg_di'],
        self.record['rsi'],
        self.record['exit_time'],
        self.record['exit_price'],
        self.record['stop_loss'],
        self.record['units'],
        self.record['stopped_out'],
        self.record['pl'],
     ]
    )

  def handle_enter(self, **kwargs):
    self.record.update(**kwargs)

  def handle_exit(self, **kwargs):
    pl = kwargs['pl']
    total_trades = self.get_cache('total_trades')
    winning_trades = self.get_cache('winning_trades')
    profit_from_trades = self.get_cache('profit_from_trades')
    max_profit = self.get_cache('max_profit')
    losing_trades = self.get_cache('losing_trades')
    loss_from_trades = self.get_cache('loss_from_trades')
    max_loss = self.get_cache('max_loss')
    longest_losing_streak = self.get_cache('longest_losing_streak')
    losing_streak_losses = self.get_cache('losing_streak_losses')
    self.set_cache('total_trades', total_trades+1)
    if pl > 0:
      self.set_cache('winning_trades', winning_trades+1)
      self.set_cache('profit_from_trades', profit_from_trades+pl)
      self.set_cache('max_profit', max(max_profit, pl))
      self.losing_amt = 0
      self.losing_streak = 0
    else:
      self.losing_amt += pl
      self.losing_streak += 1
      self.set_cache('max_loss', min(max_loss, pl))
      if self.losing_streak == 1:  # start of losing streak
        self.losing_dates = [kwargs['exit_time'], kwargs['exit_time']]
      else:
        self.losing_dates[1] = kwargs['exit_time']
        if max(longest_losing_streak, self.losing_streak) == self.losing_streak:
          self.set_cache('losing_dates', self.losing_dates)

      self.set_cache('longest_losing_streak', max(longest_losing_streak, self.losing_streak))
      self.set_cache('losing_streak_losses', min(losing_streak_losses, self.losing_amt))
      self.set_cache('losing_trades', losing_trades+1)
      self.set_cache('loss_from_trades', loss_from_trades+pl)

    self.record.update(**kwargs)
    self._record_trade()

  def conclude(self):
    total_trades = self.get_cache('total_trades')
    winning_trades = self.get_cache('winning_trades')
    profit_from_trades = self.get_cache('profit_from_trades')
    max_profit = self.get_cache('max_profit')
    losing_trades = self.get_cache('losing_trades')
    loss_from_trades = self.get_cache('loss_from_trades')
    max_loss = self.get_cache('max_loss')
    longest_losing_streak = self.get_cache('longest_losing_streak')
    losing_dates = self.get_cache('losing_dates')
    losing_streak_losses = self.get_cache('losing_streak_losses')
    self.output_file.close()

    # print(f"Final amt: {self.get_cache('acc_balance')}")
    # print(f"Total trades: {total_trades}")
    # print(f"\nWins:\nWinning Trades: {winning_trades}")
    # print(f'Win rate: {winning_trades/total_trades}')
    # print(f'Total profits: {profit_from_trades}')
    # print(f'\nLosses:\nLosing Trades: {losing_trades}')
    # print(f'Losing rate: {losing_trades/total_trades}')
    # print(f'Largest loss: {max_loss}')
    # print(f'Total losses: {loss_from_trades}')
    # print(f'losing streak {longest_losing_streak}, from {" - ".join(losing_dates)}\nlost: {losing_streak_losses}')

    print(f"{self.instrument}: {round(self.get_cache('acc_balance'), 2)}")
    print(f"Total trades: {total_trades}, WIN: {round(winning_trades/total_trades, 3)}, LOSE: {round(losing_trades/total_trades, 3)}")


def init_backtest(instrument, csv_file):
  machine = backtesting_adx.BacktestingAdx()
  return Backtester(machine, instrument, csv_file, f'{csv_file}-adx')


def tgt_backtest(year=''):
  output_file = open(f'csv_files/total{year}.csv', 'w', newline='')
  output_writer = csv.writer(output_file)
  output_writer.writerow(
    ['Instrument', 'Acc Balance', 'Position', 'Entry Time', 'Entry Price', 'ADX', 'Pos DI', 'Neg DI', 'RSI',
     'Exit Time', 'Exit Price', 'Stop Loss', 'Units', 'Stopped Out', 'Profit/Loss']
  )
  aud = init_backtest('AUD_USD', f'aud-usd{year}')
  aud.output_writer = output_writer
  eur = init_backtest('EUR_USD', f'eur-usd{year}')
  eur.output_writer = output_writer
  nzd = init_backtest('NZD_USD', f'nzd-usd{year}')
  nzd.output_writer = output_writer
  gbp = init_backtest('GBP_USD', f'gbp-usd{year}')
  gbp.output_writer = output_writer
  inst_list = [aud, eur, nzd, gbp]
  balance = 200
  count = set()
  while len(count) < 4:
    for inst in inst_list:
      inst.set_cache('acc_balance', balance)
      try:
        inst.run()
      except:
        count.add(inst.instrument)
      balance = inst.get_cache('acc_balance')
  print(f'final balance: {balance}')
  output_file.close()
  return balance


def run_backtest(instrument, csv_file, output_file=None):
  try:
    machine = backtesting_adx.BacktestingAdx()
    if output_file is None:
      output_file = f'{csv_file}-adx'
    bt = Backtester(machine, instrument, csv_file, output_file)
    bt.backtest()
  except Exception as e:
    print(e)

  # try:
  #   print(f'RSI')
  #   machine = backtesting_rsi.BacktestingRsi()
  #   bt = Backtester(machine, instrument, csv_file, f'{csv_file}-rsi')
  #   bt.backtest()
  # except:
  #   pass


def chart_vars():
  output_file = open(f'csv_files/plotted-all.csv', 'w', newline='')
  output_writer = csv.writer(output_file)
  output_writer.writerow(
    ['ADX Condition', 'Time Period', 'ATR Multiple', 'Bullish RSI', 'Bearish RSI', '7 Yr', '10 Yr']
  )

  count = 0
  time_period = [10,12,14]
  trailing_exit = [0.5, 1, 2]
  bull_rsi = [50, 55, 60, 65]
  bear_rsi = [50, 45, 40, 35]
  adx_condition = [27, 28, 29, 30, 31, 32]
  total = len(time_period) * len(trailing_exit) * len(bull_rsi) * len(bear_rsi) * len(adx_condition)
  res = []
  for adx in adx_condition:
    for period in time_period:
      for trailing in trailing_exit:
        for bull in bull_rsi:
          for bear in bear_rsi:
            count += 1
            print(f'{count}/{total}')
            vars = {
              'adx_condition': adx,
              'period': period,
              'atr_multiple': trailing,
              'bull_rsi': bull,
              'bear_rsi': bear
            }
            vars['10y'] = tgt_backtest(vars, '-10y')
            vars['7y'] = tgt_backtest(vars, '-7y-exc')
            res.append(vars)

  for r in sorted(res, key=lambda k: k['10y'], reverse=True):
    output_writer.writerow(
      [r['adx_condition'], r['period'], r['atr_multiple'], r['bull_rsi'], r['bear_rsi'], r['7y'], r['10y']]
    )
  output_file.close()
