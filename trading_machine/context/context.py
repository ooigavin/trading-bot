class BaseContext:
  """
  base context class for state design pattern with state machine
  """

  def __init__(self, machine):
    self.machine = machine

  def get_state(self):
    return self.machine.curr_state

  def trigger_transition(self, state, event):
    self.machine.handle_transition(state, event)
