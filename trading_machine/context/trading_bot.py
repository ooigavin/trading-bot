import json
import os
import time

from app import cache

from misc import telegram_utils as tele

from trading_machine.context.context import BaseContext


class TradingBot(BaseContext):

  def __init__(self, machine, instrument, env):
    super().__init__(machine)
    instrument_key = f'{env}_{instrument}'
    self.instrument = instrument
    self.env = env
    self._cache = cache
    values = cache.get(instrument_key)
    if values is None:
      self._instrument_values = {}
    else:
      self._instrument_values = json.loads(values)

  def start(self):
    msg = ''
    start = time.time()
    while self.get_state().name != 'END':
      new = f'{self.get_state().name}\n'
      print(new)
      msg += new
      self.get_state().handle(self)

    # add cache values
    msg = f'{self.env}_{self.instrument}: {round(time.time()-start, 2)}\n' + msg
    msg += f'\nacc balance: {float(self.get_cache("acc_balance"))}'
    msg += f'\ntrend: {self._instrument_values["market_trend"]}'
    msg += f'\nlast close: {self.get_cache("prices")[-1]["candle"]["c"]}'
    msg += f'\nstma: {self._instrument_values["stma"]}'
    msg += f'\nltma: {self._instrument_values["ltma"]}'
    msg += f'\natr: {self._instrument_values["atr"]}'
    msg += f'\nadx: {self._instrument_values["adx"]}'
    msg += f'\nrsi: {self._instrument_values["rsi"]}'
    if self._instrument_values['entered_trade']:
      msg += f"\n\nposition: {self._instrument_values['position']}"
      msg += f'\nentry_price: {self._instrument_values["entry_price"]}'
      msg += f'\nunits: {self._instrument_values["units"]}'
      msg += f'\nstop_loss: {self._instrument_values["stop_loss"]}'

    tele.send_bot_message(msg, os.environ.get('TELE_CHAT_ID'))

  def get_cache(self, key):
    if key == 'acc_balance':
      return self._cache.get(key)
    else:
      return self._instrument_values.get(key)

  def set_cache(self, key, value):
    if key == 'acc_balance':
      self._cache.set(key, value)
    else:
      self._instrument_values[key] = value

  def save_cache(self):
    self._cache.set(f'{self.env}_{self.instrument}', json.dumps(self._instrument_values))

  def handle_exit(self, pl):
    if float(pl) > 0:
      losses = 0
    else:
      losses = self.get_cache('losses')
      if losses:
        losses += 1
        if losses > 12:
          losses = 0
          self.set_cache('entry_allowed', False)
      else:
        losses = 1

    self.set_cache('losses', losses)
    self.save_cache()
