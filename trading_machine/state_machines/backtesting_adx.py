from trading_machine.events import Events

from trading_machine.states.backtest_start import BacktestStart
from trading_machine.states.check_entered_trade import CheckEnteredTrade
from trading_machine.states.backtest_check_trade_position import BacktestCheckTradePosition
from trading_machine.states.check_entry_adx import CheckEntryAdx
from trading_machine.states.backtest_enter import BacktestEnter
from trading_machine.states.check_exit_adx import CheckExitAdx
from trading_machine.states.backtest_exit import BacktestExit
from trading_machine.states.reset_metrics import ResetMetrics
from trading_machine.states.end import End


class BacktestingAdx:

  def __init__(self):
    self.start = BacktestStart()
    self.curr_state = self.start
    self.transition_table = {
      # update metrics
      (BacktestStart.name, Events.TREND_CONTINUATION): CheckEnteredTrade,
      (BacktestStart.name, Events.TREND_REVERSAL): ResetMetrics,
      (CheckEnteredTrade.name, Events.ENTERED_TRADE): BacktestCheckTradePosition,
      (CheckEnteredTrade.name, Events.NOT_ENTERED_TRADE): CheckEntryAdx,
      # not entered trade: look for entry
      (CheckEntryAdx.name, Events.COND_MET): BacktestEnter,
      (CheckEntryAdx.name, Events.COND_NOT_MET): End,
      (BacktestEnter.name, Events.DONE): End,
      # entered trade: look for exit
      (BacktestCheckTradePosition.name, Events.NOT_HOLDING_POSITION): ResetMetrics,
      (BacktestCheckTradePosition.name, Events.HOLDING_POSITION): CheckExitAdx,
      (CheckExitAdx.name, Events.COND_NOT_MET): End,
      (CheckExitAdx.name, Events.COND_MET): BacktestExit,
      (BacktestExit.name, Events.DONE): ResetMetrics,
      (ResetMetrics.name, Events.DONE): End,
    }

  def handle_transition(self, state, event):
    new_state = self.transition_table.get((state, event), End)
    self.curr_state = new_state()

  def reset(self):
    self.curr_state = self.start
