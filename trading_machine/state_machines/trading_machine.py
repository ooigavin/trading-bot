from trading_machine.events import Events

from trading_machine.states.start import Start
from trading_machine.states.check_entered_trade import CheckEnteredTrade
from trading_machine.states.check_entry_adx import CheckEntryAdx
from trading_machine.states.check_trade_position import CheckTradePosition
from trading_machine.states.enter import Enter
from trading_machine.states.check_exit_adx import CheckExitAdx
from trading_machine.states.exit import Exit
from trading_machine.states.reset_metrics import ResetMetrics
from trading_machine.states.end import End


class TradingMachine:

  def __init__(self):
    self.curr_state = Start()
    self.transition_table = {
      # update metrics
      (Start.name, Events.TREND_CONTINUATION): CheckEnteredTrade,
      (Start.name, Events.TREND_REVERSAL): ResetMetrics,
      (CheckEnteredTrade.name, Events.ENTERED_TRADE): CheckTradePosition,
      (CheckEnteredTrade.name, Events.NOT_ENTERED_TRADE): CheckEntryAdx,
      # not entered trade: look for entry
      (CheckEntryAdx.name, Events.COND_MET): Enter,
      (CheckEntryAdx.name, Events.COND_NOT_MET): End,
      (Enter.name, Events.DONE): End,
      # entered trade: look for exit
      (CheckTradePosition.name, Events.NOT_HOLDING_POSITION): ResetMetrics,
      (CheckTradePosition.name, Events.HOLDING_POSITION): CheckExitAdx,
      (CheckExitAdx.name, Events.COND_NOT_MET): End,
      (CheckExitAdx.name, Events.COND_MET): Exit,
      (Exit.name, Events.DONE): ResetMetrics,
      (ResetMetrics.name, Events.DONE): End,
    }

  def handle_transition(self, state, event):
    new_state = self.transition_table.get((state, event), End)
    print(f'prev state: {self.curr_state.name} --> ({event}) -->  new state: {new_state.name}')
    self.curr_state = new_state()
