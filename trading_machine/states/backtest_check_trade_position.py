from trading_machine.states.base_state import State
from trading_machine.events import Events
from trading_machine.trading_constants import Position, STOPLOSS_PREMIUM


class BacktestCheckTradePosition(State):
  """
  check if we are still holding onto trade
  if we are not, we were forced to exit the trade
  else check for exit trigger
  """
  name = 'BACKTEST_CHECK_TRADE_POSITION'

  def handle(self, context):
    stop_loss = context.get_cache('stop_loss')
    position = context.get_cache('position')
    prices = context.get_cache('prices')
    last_close = prices[-1]['candle']['c']
    entry_price = context.get_cache('entry_price')
    units = context.get_cache('units')

    if position == Position.LONG:
      stopped = last_close < stop_loss
      premium = -STOPLOSS_PREMIUM[context.instrument]
    else:
      stopped = last_close > stop_loss
      premium = STOPLOSS_PREMIUM[context.instrument]

    if stopped:
      acc_balance = context.get_cache('acc_balance')
      pl = (stop_loss-entry_price+premium)*units
      context.handle_exit(
        exit_time=prices[-1]['time'],
        exit_price=stop_loss,
        stopped_out=True,
        pl=pl
      )
      context.set_cache('acc_balance', acc_balance + pl)
      context.trigger_transition(self.name, Events.NOT_HOLDING_POSITION)
    else:
      context.trigger_transition(self.name, Events.HOLDING_POSITION)
