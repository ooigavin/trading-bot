from trading_machine.trading_constants import PIP_VALUE, MarketSentiment, Position, RISK_PERCENTAGE, MIN_DIST
from trading_machine.states.base_state import State
from trading_machine.events import Events


class BacktestEnter(State):
  """
  enter the trade
  update the cache
  """
  name = 'BACKTEST_ENTER'

  def handle(self, context):
    acc_balance = context.get_cache('acc_balance')
    market_trend = context.get_cache('market_trend')
    adx = context.get_cache('adx')
    rsi = context.get_cache('rsi')
    pos_di = context.get_cache('pos_di')
    neg_di = context.get_cache('neg_di')
    ltma = context.get_cache('ltma')
    atr = context.get_cache('atr')
    prices = context.get_cache('prices')
    bid = prices[-1]['bid']
    ask = prices[-1]['ask']
    last_close = prices[-1]['candle']['c']

    risk = acc_balance * RISK_PERCENTAGE
    if market_trend == MarketSentiment.BULL:
      entry = ask
      stop_loss = round(ltma - atr/2, 4)
      position = Position.LONG
    else:
      entry = bid
      stop_loss = round(ltma + atr/2, 4)
      position = Position.SHORT

    if position == Position.SHORT:
      distance = max(stop_loss-entry, MIN_DIST[context.instrument]) * -10000
    else:
      distance = max(entry-stop_loss, MIN_DIST[context.instrument]) * 10000
    units = int(risk/(distance*PIP_VALUE))

    context.set_cache('entered_trade', True)
    context.set_cache('stop_loss', stop_loss)
    context.set_cache('entry_price', entry)
    context.set_cache('units', units)
    context.set_cache('position', position)
    context.save_cache()

    context.handle_enter(
      entry_price=entry,
      entry_time=prices[-1]['time'],
      units=units,
      position=position,
      last_close=last_close,
      adx=adx,
      rsi=rsi,
      pos_di=pos_di,
      neg_di=neg_di,
      stop_loss=stop_loss
    )

    context.trigger_transition(self.name, Events.DONE)
