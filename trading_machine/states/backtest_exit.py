from trading_machine.states.base_state import State
from trading_machine.events import Events
from trading_machine.trading_constants import Position


class BacktestExit(State):
  """
  trade has been exited
  change entered_trade state & take profits
  """
  name = 'BACKTEST_EXIT'

  def handle(self, context):
    prices = context.get_cache('prices')
    position = context.get_cache('position')
    units = context.get_cache('units')
    acc_balance = context.get_cache('acc_balance')
    entry_price = context.get_cache('entry_price')

    bid = prices[-1]['bid']
    ask = prices[-1]['ask']
    if position == Position.LONG:
      exit_price = ask
    else:
      exit_price = bid

    pl = (exit_price-entry_price)*units
    context.handle_exit(
      exit_time=prices[-1]['time'],
      exit_price=exit_price,
      stopped_out=False,
      pl=pl
    )
    context.set_cache('entered_trade', False)
    context.set_cache('acc_balance', acc_balance+pl)
    context.save_cache()

    context.trigger_transition(self.name, Events.DONE)
