from trading_machine.states.base_state import State
from trading_machine.events import Events

from trading_machine.trading_constants import LTMA_PERIOD, STMA_PERIOD, ADX_PERIOD, RSI_PERIOD, MarketSentiment
from trading_machine.trading_indicators import new_moving_average, check_for_rejection, new_adx, new_rsi


class BacktestStart(State):

  name = 'BACKTEST_START'

  @staticmethod
  def _update_market_structure(context):
    prices = context.get_cache('prices')
    prev, curr = prices[-2]['candle'], prices[-1]['candle']
    market_trend = context.get_cache('market_trend')
    rejections = context.get_cache('strong_rejections')
    stma = context.get_cache('stma')
    pos_di = context.get_cache('pos_di')
    neg_di = context.get_cache('neg_di')

    # dont reset metrics if trade has alr been entered
    if context.get_cache('entered_trade'):
      return False

    if pos_di > neg_di:
      new_trend = MarketSentiment.BULL
    else:
      new_trend = MarketSentiment.BEAR

    context.set_cache('market_trend', new_trend)
    if new_trend != market_trend:
      return True

    if check_for_rejection(prev, curr, new_trend, stma):
      context.set_cache('strong_rejections', rejections + 1)

    return False

  def handle(self, context):
    prices = context.get_cache('prices')
    newest_price = context.fetch_candles(1)[-1]
    if newest_price['time'] == prices[-1]['time']:
      context.trigger_transition(self.name, Events.TREND_CONTINUATION)  # CheckEnteredTrade
      return

    prices.append(newest_price)
    prev, curr = prices[-2]['candle'], prices[-1]['candle']
    old_ltma = context.get_cache('ltma')
    old_stma = context.get_cache('stma')
    old_atr = context.get_cache('atr')
    old_smoothed_tr = context.get_cache('smoothed_tr')
    old_smoothed_pos_dm = context.get_cache('smoothed_pos_dm')
    old_smoothed_neg_dm = context.get_cache('smoothed_neg_dm')
    old_adx = context.get_cache('adx')
    old_avg_gain = context.get_cache('avg_gain')
    old_avg_loss = context.get_cache('avg_loss')
    ltma = new_moving_average(old_ltma, prices, LTMA_PERIOD)
    stma = new_moving_average(old_stma, prices[-(STMA_PERIOD + 1):], STMA_PERIOD)
    pos_di, smoothed_pos_dm, neg_di, smoothed_neg_dm, atr, smoothed_tr, adx_val = new_adx(prev, curr, old_smoothed_pos_dm, old_smoothed_neg_dm, old_atr, old_smoothed_tr, old_adx, ADX_PERIOD)
    avg_gain, avg_loss, rsi_val = new_rsi(prev, curr, old_avg_gain, old_avg_loss, RSI_PERIOD)
    prices.pop(0)  # remove oldest value from list of prices

    # update indicators
    context.set_cache('ltma', ltma)
    context.set_cache('stma', stma)
    context.set_cache('atr', atr)
    context.set_cache('smoothed_tr', smoothed_tr)
    context.set_cache('smoothed_pos_dm', smoothed_pos_dm)
    context.set_cache('smoothed_neg_dm', smoothed_neg_dm)
    context.set_cache('pos_di', pos_di)
    context.set_cache('neg_di', neg_di)
    context.set_cache('adx', adx_val)
    context.set_cache('avg_gain', avg_gain)
    context.set_cache('avg_loss', avg_loss)
    context.set_cache('rsi', rsi_val)

    # check for market structure
    trend_reversal = self._update_market_structure(context)

    # save cache
    context.save_cache()

    if trend_reversal:
      context.trigger_transition(self.name, Events.TREND_REVERSAL)  # ResetMetrics
    else:
      context.trigger_transition(self.name, Events.TREND_CONTINUATION)  # CheckEnteredTrade
