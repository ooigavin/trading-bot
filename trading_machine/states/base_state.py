class State:
  name = None

  def __init__(self):
    if not self.name:
      raise ValueError('name attribute is required for state class')

  def handle(self, context):
    pass
