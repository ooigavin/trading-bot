from trading_machine.states.base_state import State
from trading_machine.events import Events


class CheckEnteredTrade(State):
  """
  check if a trade has been entered
  if yes move to CHECK_TRADE_POSITION
  if no move to CHECK_TRADE_SETUP
  """
  name = 'CHECK_ENTERED_TRADE'

  def handle(self, context):
    entered_trade = context.get_cache('entered_trade')
    if entered_trade:
      context.trigger_transition(self.name, Events.ENTERED_TRADE)
    else:
      context.trigger_transition(self.name, Events.NOT_ENTERED_TRADE)
