import os

from misc import telegram_utils as tele

from trading_machine.trading_constants import MarketSentiment, ADX_ENTRY, BULLISH_RSI_FLOOR, BEARISH_RSI_CEILING, RSI_OVERBOUGHT, RSI_OVERSOLD
from trading_machine.states.base_state import State
from trading_machine.events import Events


class CheckEntryAdx(State):
  """
  if strong rejection
  adx > 30
  if price broke below/above stma
  """
  name = 'CHECK_ENTRY_ADX'

  def handle(self, context):
    market_trend = context.get_cache('market_trend')
    ltma = context.get_cache('ltma')
    rsi = context.get_cache('rsi')
    close_price = context.get_cache('prices')[-1]['candle']['c']
    if market_trend == MarketSentiment.BEAR:
      price_condition = close_price < ltma
      rsi_condition = RSI_OVERSOLD < rsi < BEARISH_RSI_CEILING
    else:
      price_condition = ltma < close_price
      rsi_condition = BULLISH_RSI_FLOOR < rsi < RSI_OVERBOUGHT

    adx_condition = context.get_cache('adx') >= ADX_ENTRY

    allowed = context.get_cache('entry_allowed')

    if rsi_condition and price_condition and adx_condition:
      if not allowed:
        tele.send_bot_message(f'Entry not allowed: {context.env}-{context.instrument}', os.environ.get('TELE_CHAT_ID'))
        context.trigger_transition(self.name, Events.COND_NOT_MET)
      else:
        context.trigger_transition(self.name, Events.COND_MET)
    else:
      context.trigger_transition(self.name, Events.COND_NOT_MET)
