from trading_machine.states.base_state import State
from trading_machine.events import Events


class CheckEntryTriggers(State):
  """
  check if entry triggers have been met
  if yes move to ENTER
  else move to END
  """
  name = 'CHECK_ENTRY_TRIGGERS'

  def handle(self, context):
    # get the most recent candle stick, if it closes below the 20 period MA enter
    stma = context.get_cache('stma')
    # get the mos
    close_price = context.get_cache('prices')[-1]['candle']['c']
    if close_price < stma:
      context.trigger_transition(self.name, Events.COND_MET)
    else:
      context.trigger_transition(self.name, Events.COND_NOT_MET)
