from trading_machine.states.base_state import State
from trading_machine.events import Events
from trading_machine.trading_constants import Position, RSI_OVERSOLD, RSI_OVERBOUGHT


class CheckExitAdx(State):
  """
  check if exit condition was met
  if met, move to exit state
  else move to end
  """
  name = 'CHECK_EXIT_ADX'

  def handle(self, context):
    atr = context.get_cache('atr')
    position = context.get_cache('position')
    prices = context.get_cache('prices')
    curr = prices[-1]['candle']['c']
    ltma = context.get_cache('ltma')
    rsi = context.get_cache('rsi')

    if position == Position.LONG:
      price_condition = curr < (ltma - atr)
      rsi_condition = rsi > RSI_OVERBOUGHT
    else:
      price_condition = curr > (ltma + atr)
      rsi_condition = rsi < RSI_OVERSOLD

    # exit if the curr closing price + atr is under the ltma
    if price_condition or rsi_condition:
      context.trigger_transition(self.name, Events.COND_MET)
    else:
      context.trigger_transition(self.name, Events.COND_NOT_MET)
