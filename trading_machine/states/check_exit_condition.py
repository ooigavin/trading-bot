from trading_machine.states.base_state import State
from trading_machine.events import Events


class CheckExitCondition(State):
  """
  check if exit condition was met
  if met, move to exit state
  else move to end
  """
  name = 'CHECK_EXIT_CONDITION'

  def handle(self, context):
    atr = context.get_cache('atr')
    prices = context.get_cache('prices')
    curr = prices[-1]['candle']['c']
    ltma = context.get_cache('ltma')

    # exit if the curr closing price + atr is under the ltma
    if (curr+atr) < ltma:
      context.trigger_transition(self.name, Events.COND_MET)
    else:
      context.trigger_transition(self.name, Events.COND_NOT_MET)
