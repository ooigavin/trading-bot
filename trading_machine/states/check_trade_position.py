import os

from trading_machine.states.base_state import State
from trading_machine.events import Events

from misc import oanda_utils as oa
from misc import telegram_utils as tele


class CheckTradePosition(State):
  """
  check if we are still holding onto trade
  if we are not, we were forced to exit the trade
  else check for exit trigger
  """
  name = 'CHECK_TRADE_POSITION'

  def handle(self, context):

    position_resp = oa.get_position(context.instrument, context.env)
    position = position_resp['position']
    trade_position = context.get_cache('position').lower()
    if position[trade_position]['units'] == '0':
      inst = f'{context.env}_{context.instrument}'
      msg = f'Shit, stopped out {inst}'
      context.handle_exit(-1)  # set to negative value to indicate a loss, actual value not impt
      tele.send_bot_message(msg, os.environ.get('TELE_CHAT_ID'))
      context.trigger_transition(self.name, Events.NOT_HOLDING_POSITION)
    else:
      context.trigger_transition(self.name, Events.HOLDING_POSITION)
