from trading_machine.states.base_state import State


class End(State):
  """
  terminal state
  """
  name = 'END'

  def handle(self, context):
    return
