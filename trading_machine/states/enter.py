import os

from trading_machine.trading_constants import MarketSentiment, PIP_VALUE, Position, RISK_PERCENTAGE, MIN_DIST, Env
from trading_machine.states.base_state import State
from trading_machine.events import Events

from misc import telegram_utils as tele
from misc import oanda_utils as oa


class Enter(State):
  """
  enter the trade
  update the cache
  """
  name = 'ENTER'

  def handle(self, context):
    acc_balance = context.get_cache('acc_balance')
    market_trend = context.get_cache('market_trend')
    ltma = context.get_cache('ltma')
    atr = context.get_cache('atr')
    prices = context.get_cache('prices')
    bid = prices[-1]['bid']
    ask = prices[-1]['ask']

    if context.env == Env.DEMO:
      risk = 2
    else:
      risk = float(acc_balance) * RISK_PERCENTAGE
    if market_trend == MarketSentiment.BULL:
      entry = ask
      stop_loss = round(ltma - atr/2, 4)
      position = Position.LONG
    else:
      entry = bid
      stop_loss = round(ltma + atr/2, 4)
      position = Position.SHORT

    if position == Position.SHORT:
      distance = max(stop_loss-entry, MIN_DIST[context.instrument]) * -10000
    else:
      distance = max(entry-stop_loss, MIN_DIST[context.instrument]) * 10000
    units = int(risk/(distance*PIP_VALUE))

    create_resp = oa.create_market_order(instrument=context.instrument, stop_loss=stop_loss, units=units, env=context.env)
    if 'orderFillTransaction' in create_resp:
      entry_price = create_resp['orderFillTransaction']['price']
      context.set_cache('entered_trade', True)
      context.set_cache('stop_loss', stop_loss)
      context.set_cache('entry_price', entry_price)
      context.set_cache('units', units)
      context.set_cache('position', position)
      context.save_cache()
      inst = f'{context.env}_{context.instrument}'
      msg = f'Entered {position} trade {inst}\nprice: {entry_price}\nstop loss: {stop_loss}\nunits: {units}'
    else:
      if 'orderCancelTransaction' in create_resp:
        reason = create_resp['orderCancelTransaction']['reason']
      elif 'orderRejectTransaction' in create_resp:
        reason = create_resp['orderRejectTransaction']['rejectReason']
      else:
        reason = list(create_resp.keys())
      msg = f'Failed to enter trade: \nunits: {units}, stoploss: {stop_loss}\n{reason}'

    tele.send_bot_message(msg, os.environ.get('TELE_CHAT_ID'))
    context.trigger_transition(self.name, Events.DONE)
