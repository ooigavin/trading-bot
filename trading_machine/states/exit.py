import os

from trading_machine.states.base_state import State
from trading_machine.events import Events
from trading_machine.trading_constants import Position

from misc import telegram_utils as tele
from misc import oanda_utils as oa


class Exit(State):
  """
  trade has been exited
  change entered_trade state & take profits
  """
  name = 'EXIT'

  def handle(self, context):
    position = context.get_cache('position')
    if position == Position.LONG:
      close_resp = oa.close_position(context.instrument, long=True, env=context.env)
    else:
      close_resp = oa.close_position(context.instrument, short=True, env=context.env)

    position = position.lower()
    if f'{position}OrderFillTransaction' in close_resp:
      fill_trans = close_resp[f'{position}OrderFillTransaction']
      entry = context.get_cache('entry_price')
      inst = f'{context.env}_{context.instrument}'
      msg = f'Exited {position} trade {inst}\nentry: {entry}\nexit: {fill_trans["price"]}\nunits: {fill_trans["units"]}\nPL: {fill_trans["pl"]}'
      context.handle_exit(fill_trans['pl'])
    else:
      if f'{position}OrderCancelTransaction' in close_resp:
        reason = close_resp[f'{position}OrderCancelTransaction']['reason']
      elif f'{position}OrderRejectTransaction' in close_resp:
        reason = close_resp[f'{position}OrderRejectTransaction']['rejectReason']
      else:
        reason = list(close_resp.keys())
      msg = f'Unable to exit trade: {reason}'

    tele.send_bot_message(msg, os.environ.get('TELE_CHAT_ID'))
    context.trigger_transition(self.name, Events.DONE)
