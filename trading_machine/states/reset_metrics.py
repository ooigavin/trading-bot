from trading_machine.states.base_state import State
from trading_machine.events import Events


class ResetMetrics(State):
  """
  reset metrics to some default values when market structure breaks down
  metrics to reset:
  - total_candles
  - closed_below_ma
  - strong_rejections
  - market_structure_met
  """
  name = 'RESET_METRICS'

  def handle(self, context):
    context.set_cache('strong_rejections', 0)
    context.set_cache('entry_price', 0)
    context.set_cache('units', 0)
    context.set_cache('stop_loss', 0)
    context.set_cache('entered_trade', False)
    context.save_cache()

    context.trigger_transition(self.name, Events.DONE)
