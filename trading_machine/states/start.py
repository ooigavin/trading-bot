from misc.oanda_utils import get_acc_size, fetch_candles

from trading_machine.states.base_state import State
from trading_machine.events import Events

from trading_machine.trading_constants import LTMA_PERIOD, STMA_PERIOD, ADX_PERIOD, RSI_PERIOD, TimePeriods, MarketSentiment
from trading_machine.trading_indicators import moving_average, new_moving_average, check_for_rejection, adx, new_adx, rsi, new_rsi


class Start(State):
  """
  fetch values & update the cached values
  cached_values = {
    entered_trade
    total_candles
    closed_below_ma
    strong_rejections
    market_structure_met
    prices
    ltma
    stma
    atr
  }
  """
  name = 'START'

  @staticmethod
  def _update_market_structure(context):
    prices = context.get_cache('prices')
    prev, curr = prices[-2]['candle'], prices[-1]['candle']
    market_trend = context.get_cache('market_trend')
    rejections = context.get_cache('strong_rejections')
    stma = context.get_cache('stma')
    pos_di = context.get_cache('pos_di')
    neg_di = context.get_cache('neg_di')

    # dont reset metrics if trade has alr been entered
    if context.get_cache('entered_trade'):
      return False

    if pos_di > neg_di:
      new_trend = MarketSentiment.BULL
    else:
      new_trend = MarketSentiment.BEAR

    context.set_cache('market_trend', new_trend)
    if new_trend != market_trend:
      return True

    if check_for_rejection(prev, curr, new_trend, stma):
      context.set_cache('strong_rejections', rejections+1)

    return False

  def _update_candle_stick_data(self, context):
    total_candles = context.get_cache('total_candles')
    closed_below_ma = context.get_cache('closed_below_ma')
    rejections = context.get_cache('strong_rejections')
    prices = context.get_cache('prices')
    ltma = context.get_cache('ltma')
    stma = context.get_cache('stma')
    curr = prices[-1]['candle']
    prev = prices[-2]['candle']

    # increment the no of candles
    context.set_cache('total_candles', total_candles+1)
    # check for closed below ma
    if curr['c'] < ltma:
      context.set_cache('closed_below_ma', closed_below_ma+1)
    # check for rejections
    if check_for_rejection(prev, curr, MarketSentiment.BULL, stma):
      context.set_cache('strong_rejections', rejections+1)

  def handle(self, context):
    # if empty, fetch all prices on first start up
    prices = context.get_cache('prices')
    if prices is None:
      new_prices = fetch_candles(TimePeriods.H1, context.instrument, LTMA_PERIOD, context.env)
      stma = moving_average(new_prices[-STMA_PERIOD:])
      ltma = moving_average(new_prices)
      pos_di, smoothed_pos_dm, neg_di, smoothed_neg_dm, atr, smoothed_tr, adx_val = adx(new_prices[-(ADX_PERIOD*2):])
      avg_gain, avg_loss, rsi_val = rsi(new_prices[-RSI_PERIOD+1:])

      context.set_cache('entered_trade', False)
      context.set_cache('entry_allowed', False)
      context.set_cache('strong_rejections', 0)
      context.set_cache('position', None)
      context.set_cache('entry_price', 0)
      context.set_cache('units', 0)
      context.set_cache('stop_loss', 0)
      context.set_cache('losses', 0)
      context.set_cache('prices', new_prices)
      if pos_di > neg_di:
        context.set_cache('market_trend', MarketSentiment.BULL)
      else:
        context.set_cache('market_trend', MarketSentiment.BEAR)
    else:
      newest_price = fetch_candles(TimePeriods.H1, context.instrument, 1, context.env)[-1]
      if newest_price['time'] == prices[-1]['time']:
        context.trigger_transition(self.name, Events.TREND_CONTINUATION)  # CheckEnteredTrade
        return
      else:
        prices.append(newest_price)
        prev, curr = prices[-2]['candle'], prices[-1]['candle']
        old_ltma = context.get_cache('ltma')
        old_stma = context.get_cache('stma')
        old_atr = context.get_cache('atr')
        old_smoothed_tr = context.get_cache('smoothed_tr')
        old_smoothed_pos_dm = context.get_cache('smoothed_pos_dm')
        old_smoothed_neg_dm = context.get_cache('smoothed_neg_dm')
        old_adx = context.get_cache('adx')
        old_avg_gain = context.get_cache('avg_gain')
        old_avg_loss = context.get_cache('avg_loss')
        ltma = new_moving_average(old_ltma, prices, LTMA_PERIOD)
        stma = new_moving_average(old_stma, prices[-(STMA_PERIOD+1):], STMA_PERIOD)
        pos_di, smoothed_pos_dm, neg_di, smoothed_neg_dm, atr, smoothed_tr, adx_val = new_adx(prev, curr, old_smoothed_pos_dm, old_smoothed_neg_dm, old_atr, old_smoothed_tr, old_adx, ADX_PERIOD)
        avg_gain, avg_loss, rsi_val = new_rsi(prev, curr, old_avg_gain, old_avg_loss, RSI_PERIOD)
        prices.pop(0)  # remove oldest value from list of prices

    # update indicators
    context.set_cache('ltma', ltma)
    context.set_cache('stma', stma)
    context.set_cache('atr', atr)
    context.set_cache('smoothed_tr', smoothed_tr)
    context.set_cache('smoothed_pos_dm', smoothed_pos_dm)
    context.set_cache('smoothed_neg_dm', smoothed_neg_dm)
    context.set_cache('pos_di', pos_di)
    context.set_cache('neg_di', neg_di)
    context.set_cache('adx', adx_val)
    context.set_cache('avg_gain', avg_gain)
    context.set_cache('avg_loss', avg_loss)
    context.set_cache('rsi', rsi_val)

    # check for market structure
    trend_reversal = self._update_market_structure(context)

    # update acc balance & save cache
    acc = get_acc_size(context.env)
    context.set_cache('acc_balance', acc)
    context.save_cache()

    if trend_reversal:
      context.trigger_transition(self.name, Events.TREND_REVERSAL)  # ResetMetrics
    else:
      context.trigger_transition(self.name, Events.TREND_CONTINUATION)  # CheckEnteredTrade
