class SimpleEnum:

  @classmethod
  def all(cls):
    return [
      val for key, val in cls.__dict__.items()
      if key not in ['__module__', '__doc__']
    ]


class Instrument(SimpleEnum):
  EUR_USD = 'EUR_USD'
  AUD_USD = 'AUD_USD'
  NZD_USD = 'NZD_USD'
  GBP_USD = 'GBP_USD'


MIN_DIST = {
  Instrument.EUR_USD: 0.0015,
  Instrument.GBP_USD: 0.003,
  Instrument.AUD_USD: 0.0015,
  Instrument.NZD_USD: 0.003
}

STOPLOSS_PREMIUM = {
  'EUR_USD': 0.00012,
  'AUD_USD': 0.0005,
  'NZD_USD': 0.0001,
  'GBP_USD': 0.0005,
}


class TimePeriods(SimpleEnum):
  H1 = 'H1'


class MarketSentiment(SimpleEnum):
  BULL = 'BULLISH'
  BEAR = 'BEARISH'


class Position(SimpleEnum):
  LONG = 'LONG'
  SHORT = 'SHORT'


class Env(SimpleEnum):
  LIVE = 'LIVE'
  DEMO = 'DEMO'


STMA_PERIOD = 20
LTMA_PERIOD = 50
ATR_PERIOD = 20
ADX_PERIOD = 10
RSI_PERIOD = 10

ADX_ENTRY = 27
BULLISH_RSI_FLOOR = 50
BEARISH_RSI_CEILING = 50
RSI_OVERBOUGHT = 70
RSI_OVERSOLD = 30

# only for usd pairs
PIP_VALUE = 0.0001
RISK_PERCENTAGE = 0.02
