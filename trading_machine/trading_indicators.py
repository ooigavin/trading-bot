from collections import namedtuple

from trading_machine.trading_constants import MarketSentiment


AdxValues = namedtuple('AdxValues', 'pos_di pos_dm neg_di neg_dm atr adx')


def get_hlc(price_obj):
  return (price_obj['h'] + price_obj['l'] + price_obj['c'])/3


def moving_average(prices):
  """
  returns the simple moving avg of the list of prices
  :param prices:
  :return:
  """
  size = len(prices)
  total = 0
  for p in prices:
    total += get_hlc(p['candle'])
  return round(total/size, 6)


def new_moving_average(val, prices, size):
  """
  calculates the new moving avg from its current value, taking into acc the newest & oldest values
  :param val:
  :param prices:
  :param size:
  :return:
  """
  old = get_hlc(prices[0]['candle'])
  new = get_hlc(prices[-1]['candle'])

  return round(val + (new-old)/size, 6)


def true_range(prev, curr):
  """
  calculates the true range, which is the max of:
  - current high less the current low
  - current high less the previous close (absolute)
  - current low less the previous close (absolute)
  :param prev:
  :param curr:
  :return:
  """
  m1 = curr['h'] - curr['l']
  m2 = abs(curr['h'] - prev['c'])
  m3 = abs(curr['l'] - prev['c'])

  return max(m1, m2, m3)


def average_true_range(prices):
  """
  returns the atr of the list of prices
  :param prices:
  :return:
  """
  size = len(prices) - 1  # dec by 1 to implement moving window
  total = 0

  for i in range(size):
    total += true_range(prices[i]['candle'], prices[i+1]['candle'])

  return round(total/size, 4)


def new_atr(old_atr, prev, curr, size):
  tr = true_range(prev, curr)
  atr = ((old_atr*(size-1)) + tr)/size

  return atr, tr


def check_for_rejection(prev, curr, market_sentiment, ma):
  # look for bullish/bearish engulfing candle pattern, as long as size is > 90% of prev candle
  def _check_bullish(candle):
    return candle['c'] > candle['o']

  def _check_bearish(candle):
    return candle['c'] < candle['o']

  # return false if open close values are the same
  if prev['c'] == prev['o'] or curr['c'] == curr['o']:
    return False

  # return false if candle is too small
  if abs(prev['c'] - prev['o']) < 0.0005:
    return False

  if market_sentiment == MarketSentiment.BULL:
    bull_candle, bear_candle = curr, prev
    within_window = prev['c'] <= ma
  else:
    bull_candle, bear_candle = prev, curr
    within_window = prev['c'] >= ma

  candle_percentage = abs(curr['c'] - curr['o'])/abs(prev['c'] - prev['o'])

  return within_window and _check_bearish(bear_candle) and _check_bullish(bull_candle) and (candle_percentage > 0.9)


def adx(prices):
  size = int(len(prices)/2)
  pos_di, smoothed_pos_dm, neg_di, smoothed_neg_dm, atr, smoothed_tr = dmi(prices[:size+1])
  total_dx = dx(pos_di, neg_di)

  for i in range(size-1):
    prev, curr = prices[size+i]['candle'], prices[size+i+1]['candle']
    pos_di, smoothed_pos_dm, neg_di, smoothed_neg_dm, atr, smoothed_tr = new_dmi(prev, curr, smoothed_pos_dm, smoothed_neg_dm, atr, smoothed_tr, size)
    new_dx = dx(pos_di, neg_di)
    total_dx += new_dx

  adx_val = total_dx/size
  return round(pos_di, 4), round(smoothed_pos_dm, 4), round(neg_di, 4), round(smoothed_neg_dm, 4), round(atr, 4), round(smoothed_tr, 4), round(adx_val, 4)


def new_adx(prev, curr, old_pos_dm, old_neg_dm, old_atr, old_tr, old_adx, size):
  pos_di, pos_dm, neg_di, neg_dm, atr, smoothed_tr = new_dmi(prev, curr, old_pos_dm, old_neg_dm, old_atr, old_tr, size)
  new_dx = dx(pos_di, neg_di)
  adx_val = ((old_adx*(size-1))+new_dx)/size
  return round(pos_di, 4), round(pos_dm, 4), round(neg_di, 4), round(neg_dm, 4), round(atr, 4), round(smoothed_tr, 4),  round(adx_val, 4)


def dx(pos_di, neg_di):
  ratio = (pos_di-neg_di)/(pos_di+neg_di)
  return abs(ratio)*100


def dmi(prices):
  size = len(prices) - 1
  smoothed_pos_dm = 0
  smoothed_neg_dm = 0
  smoothed_tr = 0

  for i in range(size):
    prev, curr = prices[i]['candle'], prices[i+1]['candle']
    pos, neg = dm(prev, curr)
    tr = true_range(prev, curr)
    smoothed_tr += tr
    smoothed_neg_dm += neg
    smoothed_pos_dm += pos

  atr = smoothed_tr/size
  pos_di = (smoothed_pos_dm*100)/smoothed_tr
  neg_di = (smoothed_neg_dm*100)/smoothed_tr
  return pos_di, smoothed_pos_dm, neg_di, smoothed_neg_dm, atr, smoothed_tr


def new_dmi(prev, curr, old_pos_dm, old_neg_dm, old_atr, old_smoothed_tr, size):
  atr, tr = new_atr(old_atr, prev, curr, size)
  new_pos_dm, new_neg_dm = dm(prev, curr)

  smoothed_tr = old_smoothed_tr - old_smoothed_tr/size + tr
  pos_dm = old_pos_dm - old_pos_dm/size + new_pos_dm
  neg_dm = old_neg_dm - old_neg_dm/size + new_neg_dm
  pos_di = (pos_dm*100)/smoothed_tr
  neg_di = (neg_dm*100)/smoothed_tr

  return pos_di, pos_dm, neg_di, neg_dm, atr, smoothed_tr


def dm(prev, curr):
  up = curr['h'] - prev['h']
  down = prev['l'] - curr['l']

  if up > 0 and up > down:
    pos = up
  else:
    pos = 0

  if down > 0 and down > up:
    neg = down
  else:
    neg = 0

  return pos, neg


def rsi(prices):
  avg_gain, avg_loss = rs_values(prices)
  rsi_val = calc_rsi(avg_gain, avg_loss)

  return avg_gain, avg_loss, round(rsi_val, 4)


def new_rsi(prev, curr, gain, loss, size):
  diff = curr['c'] - prev['c']
  if diff > 0:
    avg_gain = (gain*(size-1)+diff)/size
    avg_loss = (loss*(size-1))/size
  else:
    avg_gain = (gain*(size-1))/size
    avg_loss = (loss*(size-1)+abs(diff))/size

  rsi_val = calc_rsi(avg_gain, avg_loss)

  return avg_gain, avg_loss, round(rsi_val, 4)


def calc_rsi(gain, loss):
  rs = gain / loss
  return 100 - (100 / (1 + rs))


def rs_values(prices):
  size = len(prices) - 1
  total_gain = 0
  total_loss = 0
  for i in range(size):
    prev, curr = prices[i]['candle']['c'], prices[i+1]['candle']['c']
    diff = curr - prev
    if diff > 0:
      total_gain += diff
    else:
      total_loss += abs(diff)

  return total_gain/size, total_loss/size
